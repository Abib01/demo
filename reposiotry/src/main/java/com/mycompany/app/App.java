package com.mycompany.app;

import java.util.List;
import java.util.Scanner;

import com.mycompany.app.repository.InMemoryUserRepository;
import com.mycompany.app.repository.User;
import com.mycompany.app.repository.UserService;

/**
 * Hello world!
 *
 */
public class App {
	private static UserService userService = new UserService(new InMemoryUserRepository());
	
	private static Scanner scanner = new Scanner(System.in);
	
    public static void main( String[] args ) {
    	System.out.println("Choisissez une action");
    	System.out.println("1 - Display all users");
    	System.out.println("2 - Add a user");
    	
    	int choice = scanner.nextInt();
    	switch(choice) {
    	case 1 :{
    		display();
    		break;
    	}
    	case 2 :{
    		addUser();
    		break;
    	}
    	default:
    		System.out.println("Mauvais choix");
    	}
    }
    	
    	
    	private static void addUser() {
    		System.out.println("Entrez le nom de l'utilisateur");
    		String username = scanner.next();
    		User user = new User();
    		user.setName(username);
    		user = userService.add(user);
    		System.out.println("l'utilsateur"+ user.toString() + "a été enregistré");
    	}
    	
    	
    
       private static void display() {
    	List<User> users = userService.findAll();
    	if(users.isEmpty())
    		System.out.println("Aucune donnée");
    	
    	for(User user: users) {
    		System.out.println(user.toString());
    	}
    }
}
 