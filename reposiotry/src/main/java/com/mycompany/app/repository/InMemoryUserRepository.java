package com.mycompany.app.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class InMemoryUserRepository implements UserRepository{
	
	private Map<Long, User> db =new HashMap<>();
	private AtomicLong idGen = new AtomicLong(1);

	@Override
	public List<User> findAll() {
		List<User>users = new ArrayList<>();
		
		for(User user : db.values()) {
			users.add(user);
		}
		return users;
	}
	@Override
	public User add(User user) {
		user.setId(idGen.getAndIncrement());
		db.put(user.getId(), user);
		return user;
	}

}
