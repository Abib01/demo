package com.mycompany.app.repository;

import java.util.List;

public class UserService {
	private UserRepository userRepository;
	
	

	public UserService(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}
	public User add(User user) {
		return userRepository.add(user);
	}
	public List<User> findAll() {
		return userRepository.findAll();
	
	}

}
