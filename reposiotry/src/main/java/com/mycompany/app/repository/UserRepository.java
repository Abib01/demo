package com.mycompany.app.repository;

import java.util.List;

public interface UserRepository {
	List<User> findAll();
	User add(User user);

}
